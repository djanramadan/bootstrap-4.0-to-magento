define([
    'jquery',
    'Cro_TestMe/js/bootstrap.bundle.min'
  ], function($){
    'use strict';
    //Activate Carousel  
    $("#myCarousel").carousel();

    // Enable Carousel Indicators
    $(".item").click(function(){
      $("#myCarousel").carousel(1);
    });

    // Enable Carousel Controls
    $(".left").click(function(){
      $("#myCarousel").carousel("prev");
    }); 
    
    $(".right").click(function(){
      $("#myCarousel").carousel("next");
    }); 
  });

